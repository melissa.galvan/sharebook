<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Category;
use Doctrine\Persistence\ObjectManager as PersistenceObjectManager;

class CategoryFixtures extends Fixture
{

    public const CATEGORY_ONE = 'categoryOne';
    public const CATEGORY_TWO = 'categoryTwo';

    public function load(PersistenceObjectManager $manager)
    {
        $categoryOne = new Category();
        $categoryOne->setTitle("Biographie");

        $manager->persist($categoryOne);

        $categoryTwo = new Category();
        $categoryTwo->setTitle("Roman");

        $manager->persist($categoryTwo);

        $manager->flush();

        $this->addReference(self::CATEGORY_ONE, $categoryOne);
        $this->addReference(self::CATEGORY_TWO, $categoryTwo);

    }
}
