import Vue from 'vue'
import VueRouter from 'vue-router'
import AllArticlesPage from '../views/AllArticlesPage.vue'
import OneArticle from '../views/OneArticle.vue'
import HomePage from '../views/HomePage.vue'
import AboutPage from '../views/AboutPage.vue'
import OneCategory from '../views/OneCategory.vue'


Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'HomePage',
    component: HomePage
  },
  {
    path: '/tous-les-articles',
    name: 'AllArticlesPage',
    component: AllArticlesPage
  },
  {
    path: '/article/:id',
    name: 'OneArticle',
    component: OneArticle,
  },
  {
    path: '/category/:id',
    name: 'OneCategory',
    component: OneCategory,
  },
  {
    path: '/a-propos',
    name: 'AboutPage',
    component: AboutPage
  }
  // {
  // path: '/about',
  // name: 'About',
  // // route level code-splitting
  // // this generates a separate chunk (about.[hash].js) for this route
  // // which is lazy-loaded when the route is visited.
  // component: function () {
  //   return import(/* webpackChunkName: "about" */ '../views/About.vue')
  // }
  // }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
